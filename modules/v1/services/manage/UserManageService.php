<?php

namespace app\modules\v1\services\manage;

use app\modules\v1\entities\User\User;
use app\modules\v1\forms\manage\User\UserCreateForm;
use app\modules\v1\forms\manage\User\UserEditForm;
use app\modules\v1\repositories\UserRepository;

class UserManageService
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(UserCreateForm $form): User
    {
        $user = User::create(
            $form->username,
            $form->email,
            $form->password,
            $form->status
        );
        $this->repository->save($user);
        return $user;
    }

    public function edit($id, UserEditForm $form): User
    {
        $user = $this->repository->get($id);
        $user->edit(
            $form->username,
            $form->email,
            $form->status
        );
        $this->repository->save($user);

        return $user;
    }

    public function remove($id): void
    {
        $user = $this->repository->get($id);
        $this->repository->remove($user);
    }

    public function getAll()
    {
        return $this->repository->query();
    }
}