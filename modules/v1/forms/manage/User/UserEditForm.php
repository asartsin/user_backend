<?php

namespace app\modules\v1\forms\manage\User;

use app\modules\v1\entities\User\User;
use yii\base\Model;

class UserEditForm extends Model
{
    public $username;
    public $email;
    public $status;

    public $_user;

    public function __construct(User $user, $config = [])
    {
        $this->username = $user->username;
        $this->email = $user->email;
        $this->status = $user->status;
        $this->_user = $user;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['username', 'email'], 'required'],
            ['email', 'email'],
            ['status', 'integer'],
            ['email', 'string', 'max' => 255],
            [['username', 'email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->_user->id]],
        ];
    }
}