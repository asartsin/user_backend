<?php
namespace app\modules\v1\controllers;

use app\modules\v1\entities\User\User;
use app\modules\v1\forms\manage\User\UserEditForm;
use app\modules\v1\services\manage\UserManageService;
use app\modules\v1\forms\manage\User\UserCreateForm;
use Yii;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class UserController extends Controller
{
    /**
     * @var UserManageService
     */
    private $userManageService;

    /**
     * UserController constructor.
     * @param string $id
     * @param Module $module
     * @param UserManageService $userManageService
     * @param array $config
     */
    public function __construct($id, Module $module, UserManageService $userManageService, array $config = [])
    {
        $this->userManageService = $userManageService;
        parent::__construct($id, $module, $config);
    }

    /**
     * curl -X GET http://localhost/v1/user/index
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $this->userManageService->getAll(),
            'pagination' => [
                'params' => Yii::$app->getRequest()->getBodyParams(),
            ],
        ]);
    }

    /**
     * User create in system
     * curl -X POST -d 'username=vasiya&email=mail@my.com&password=qwerty&status=10' http://localhost/v1/user/create
     * @return array
     */
    public function actionCreate()
    {
        $form = new UserCreateForm();

        if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {

            $user = $this->userManageService->create($form);

            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::to(['view', 'id' => $user->id], true));

            return [
                'status' => 'success',
                'body' => $user->id,
            ];
        }

        return $form->errors;
    }

    /**
     * Deletes an existing User model.
     * curl -X DELETE http://localhost/v1/user/delete?id=xxx
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->request->isDelete) {
            $this->userManageService->remove($id);
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);

            return [
                'status' => 'success',
                'body' => $id,
            ];
        }
    }

    /**
     * User update in system
     * curl -X POST -d 'username=vasiya&email=mail@my.com&password=qwerty&status=10' http://localhost/v1/user/update?id=xxx
     * @throws yii\web\NotFoundHttpException
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);

        $form = new UserEditForm($user);
        if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {

            $user = $this->userManageService->edit($id, $form);

            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::to(['view', 'id' => $user->id], true));

            return [
                'status' => 'success',
                'body' => $user->id,
            ];
        }

        return $form->errors;
    }

    public function actionView($id)
    {
        $user = $this->findModel($id);

        if ($user) {

            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::to(['view', 'id' => $user->id], true));

            return $user;
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}